#### Check git configurations (user, email, all) ####
```sh
git config user.name
git config user.email
git config --list
```

#### Update list of git branches ####
```sh
git remote update origin --prune
```

#### Change author after commit ####
```sh
git commit --amend --author="{User Name} <bhavinbpanchani@gmail.com>"
```

#### Remove local branch ####
```
git branch -d {branch_name}
git branch -D {branch_name} 
```

#### Remove local branch (upstream) ####
[Reference Link](https://stackoverflow.com/questions/28462083/git-branch-d-branchname-throws-error-branch-branchname-not-found)
```sh
rm .git/refs/remotes/origin/feature/{branch_name}
rm .git/refs/heads/origin/feature/{branch_name}
```

- - - -

#### Check Processor type (Ubuntu) ####
```sh
cat /proc/cpuinfo | grep "model name"
```

- - - -

#### Install openjdk-7 (Ubuntu) ####
```sh
sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt-get install openjdk-7-jdk
```

#### Set Java version (Ubuntu) ####
```sh
https://dzone.com/articles/choosing-java-version-ubuntu
sudo update-alternatives --config java
sudo update-alternatives --config javac
```

#### Get default companyID (Liferay) ####
```java
  Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
```
#### Print all attributes in service pre-action (Liferay) ####
```java
for(Enumeration<String> names = httpServletRequest.getAttributeNames(); names.hasMoreElements();){
  String name = names.nextElement();
  Object value = httpServletRequest.getAttribute(name);
  System.out.println(name + " : " + value + "\n");
}
```

- - - -

#### Start pgadmin4 ####
```sh
source pgadmin4/bin/activate
$ python pgadmin4/lib/python2.7/site-packages/pgadmin4/pgAdmin4.py
```

- - - -

#### Run spring-boot ####
```sh
mvn spring-boot:run
```